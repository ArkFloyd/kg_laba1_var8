﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;



namespace Salimyanov_Laba1_var8

{
    public partial class Form1 : Form
    {
        
        Bitmap bm ;
       

        public Form1()
        {
            InitializeComponent();
            bm = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            Graphics g = Graphics.FromImage(bm);  
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread forDraw = new Thread(drawFigure);
            forDraw.Start();
        }
            void drawFigure(){
            Graphics g = Graphics.FromImage(bm);
            
            Pen a = new Pen(Color.Gray, 6);
            Pen b = new Pen(Color.Red, 7);
            Pen c = new Pen(Color.Orange, 6);
            SolidBrush fon = new SolidBrush(Color.Black);
            SolidBrush solidBrushWhite = new SolidBrush(Color.White);
            SolidBrush SolidBrushYellow = new SolidBrush(Color.Yellow);
            SolidBrush SolidBrushRed = new SolidBrush(Color.Red);
            

            Point[] trapezium =
          {
            new Point(80,200),
            new Point(65,250),
            new Point(115,250),
            new Point(100,200),
            
            };
            g.FillPolygon(SolidBrushYellow, trapezium);
            g.DrawPolygon(c, trapezium);
            g.FillRectangle(solidBrushWhite, 50,50, 80, 150); //Основное тело
            g.DrawRectangle(a, 50, 50, 80, 150);
            g.FillRectangle(solidBrushWhite, 20,160, 30, 50); //левый боковой выхлоп 
            g.DrawRectangle(a, 20, 160, 30, 50);
            g.FillRectangle(solidBrushWhite, 130,160, 30, 50); //правый боковой выхлоп
            g.DrawRectangle(a, 130, 160, 30, 50);

            Point[] triangle =
            {
            new Point(50,50),
            new Point(90,0),
            new Point(130,50),
            };
            g.DrawEllipse(b,65, 75, 50, 50); //Окно ракеты 
            g.FillPolygon(SolidBrushRed, triangle); //Голова
            g.DrawPolygon(b, triangle);
            pictureBox1.Image = bm; //Предача изображения в pictureBox1 

        }
      

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
        
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ImageFormat img = ImageFormat.Jpeg;
            saveFileDialog1.ShowDialog();
            switch (saveFileDialog1.FilterIndex)
            {
                case 1: img = ImageFormat.Jpeg; break;
                case 2: img = ImageFormat.Bmp; break;
                case 3: img = ImageFormat.Png; break;
            }
            Graphics g = Graphics.FromImage(bm);
            bm.Save(saveFileDialog1.FileName, img);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
            Graphics g = this.CreateGraphics();
            LinearGradientBrush myBrush = new LinearGradientBrush(ClientRectangle, Color.Yellow, Color.Green, LinearGradientMode.Vertical);
            Font myFont = new Font("Thaoma", 24, FontStyle.Regular);
            g.DrawString("Салимьянов Аркадий \t ПМ-249", myFont, Brushes.MediumPurple, 150, 10);
        }
    
        private void button5_Click(object sender, EventArgs e)
        {
            Thread potok = new Thread(animation);
            potok.Start();
        }
            void animation()
          {
            Graphics g = this.CreateGraphics();
            Pen v = new Pen(Color.Orange, 6);
            int step = 0;

                for (int i = 0; i < 30; i++)
                {
                step += 10;
                g.DrawEllipse(v, 347, 300 + step, 30, 50);
                Thread.Sleep(100);
                }
            }
       
    }
}
